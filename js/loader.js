const spinner = {
    interval:80,
    frames:[
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[=&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[==&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[===&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[====&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[=====&nbsp;&nbsp;&nbsp;]",
        "[&nbsp;=====&nbsp;&nbsp;]",
        "[&nbsp;&nbsp;=====&nbsp;]",
        "[&nbsp;&nbsp;&nbsp;=====]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;====]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;===]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;==]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;==]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;===]",
        "[&nbsp;&nbsp;&nbsp;&nbsp;====]",
        "[&nbsp;&nbsp;&nbsp;=====]",
        "[&nbsp;&nbsp;===== ]",
        "[&nbsp;=====&nbsp;&nbsp;]",
        "[=====&nbsp;&nbsp;&nbsp;]",
        "[====&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[===&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[==&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]",
        "[=&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]"]
}

function loader() {
    const grid = document.getElementById('loader')
    const spin = document.createElement('div');
    spin.innerText = spinner.frames[0];
    grid.appendChild(spin);
    let i = 0;
    return setInterval(() => {
        requestAnimationFrame(() => {
            spin.innerHTML = spinner.frames[++i % spinner.frames.length];  
        });
    }, spinner.interval);
}




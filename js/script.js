function toggleFullScreen(elem) {
    // ## The below if statement seems to work better ## if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
        if (elem.requestFullScreen) {
            elem.requestFullScreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullScreen) {
            elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
        return "on";
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
        return 'off';
    }
}

function help(hide = false) {
    let str = 'Liste des commandes disponibles :<br/>';
    str += '&nbsp;&nbsp;- aide : Affiche ce message d&apos;aide<br/>';
    str += '&nbsp;&nbsp;- bonjour : Formule de politesse<br/>';
    str += '&nbsp;&nbsp;- dit &lt;mot&gt;: R&eacute;p&egrave;te le mot<br/>';
    str += '&nbsp;&nbsp;- ping : Vérifie la connexion au terminal<br/>';
    str += '&nbsp;&nbsp;- cafard : Peut-&ecirc;tre un bug ?<br/>';
    str += '&nbsp;&nbsp;- flip : Pour exprimer sa col&egrave;re<br/>';
    str += '&nbsp;&nbsp;- unflip : Pour nettoyer après la col&egrave;re<br/>';
    str += '&nbsp;&nbsp;- table : Elle est belle ma table<br/>';
    str += '&nbsp;&nbsp;- <span class="' + ((hide) ? 'covenant' : '') + '">rate : Ouvre la porte</span><br/>';
    if (hide) {
        str += '<br/>Erreur lors de la récupération de certaines commandes';
    } else {
        str += '&nbsp;&nbsp;- <span class="' + ((hide) ? 'covenant' : '') + '">mirios : Affiche ce message d&apos;aide (mais sans les erreurs)</span><br/>';
        str += '&nbsp;&nbsp;- <span class="' + ((hide) ? 'covenant' : '') + '">fullscreen : Active/D&eacute;sactive le mode plein écran</span><br/>';
        str += '&nbsp;&nbsp;- <span class="' + ((hide) ? 'covenant' : '') + '">loquicom : C&apos;est moi le cr&eacute;ateur</span><br/>';
    }
    return str;
}

let table = true;
function flip(state) {
    // Si on flip et qu'il y a la table
    if (state && table) {
        table = false;
        return '(╯°□°）╯︵ ┻━┻';
    } 
    // Si on flip sans table
    else if (state) {
        return 'La table est d&eacute;j&agrave; &agrave; l&apos;envers T_T';
    }
    // Si on ne flip pas et qu'il n'y à pas la table
    else if (!state && !table) {
        table = true;
        return '┬─┬ノ( º _ ºノ)';
    }
    // Sinon on ne flip pas et la table est déjà la
    else {
        return 'La table est d&eacute;j&agrave; bien &agrave; sa place \\o/';
    }
}

function tableStatus() {
    if (table) {
        return '┬─┬';
    } else {
        return '┻━┻';
    }
}

function cmd() {
    const shell = new Cmd({
        selector: '#cmd',
        busy_text: 'Traitement...',
        unknown_cmd: 'Commande non reconnue, tapez "aide" pour avoir la liste des commandes disponibles',
        external_processor: processor
    });
    shell.setPrompt("[chell@glad.os] ➜ ");
}

function processor(input, cmd) {
    input = input.toLowerCase().replaceAll('é', 'e').replaceAll('è', 'e').replaceAll('à', 'a');
    const argc = input.split(" ");
    const command = argc.shift();
    switch (command) {
        case "aide":
            return help(true);
        case "help":
            return "Commande inconnue voulez-vous dire \"aide\" ?";
        case "ping":
            return "pong";
        case "cafard":
            return "crick crick";
        case "dit":
            return 'Chell&nbsp;:&nbsp;<b>' + argc.join(' ') + '</b>';
        case "bonjour":
            return "Bonjour à toi ^_^";
        case "table":
            return tableStatus();
        case "flip":
            return flip(true);
        case "unflip":
            return flip(false);
        case "mirios":
            return help(false);
        case "fullscreen":
            let res = toggleFullScreen(document.getElementById('cmd'));
            return "Fullscreen " + res;
        case "loquicom":
            return 'Lien vers mon Github : https://github.com/Loquicom';
        case "rate":
            new Promise((resolve, reject) => {
                var audio = new Audio('./success.mp3');
                audio.play();
            });
            return '<div class="ascii">' + $('#lock').html() + '</div><br/>Ouverture !';
        default:
            // Commande inconnue
            return false;
    }
}
